$(document).ready(function () {
  function setupAchievementsMainImage() {
    const imgSrc = $("#achievements-nav a.nav-link.active img").attr("src");
    $("#achievements-main-image").attr("src", imgSrc);
  }
  const heroCarousel = $("#hero-carousel");
  if (heroCarousel.length) {
    heroCarousel.slick({
      dots: true,
      arrows: false,
      adaptiveHeight: true,
    });
  }
  const circularCarousel = $("#circular-carousel");
  if (circularCarousel.length) {
    circularCarousel.slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      dots: true,
      arrows: true,
      adaptiveHeight: true,
      responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });
  }

  const achievementsNav = $("#achievements-nav a.nav-link");

  if (achievementsNav.length) {
    achievementsNav.click(function (e) {
      e.preventDefault();
      $("#achievements-nav a.nav-link.active").removeClass("active");
      $(this).addClass("active");
      setupAchievementsMainImage();
    });
    setupAchievementsMainImage();
  }
});
